# Questão 1 - Djanilson Alves
import cv2
import numpy as np


apple = cv2.imread('./apple.jpg', 0)
# grayApple = cv2.cvtColor(apple, cv2.COLOR_BGR2GRAY)
ret, grayApple = cv2.threshold(apple, 240, 255, cv2.THRESH_TOZERO)
cv2.imshow("GrayApple", grayApple)
cv2.imwrite("./grayApple.jpg", grayApple)

cv2.waitKey(0)
cv2.destroyAllWindows()
