import cv2
import numpy as np

image = np.zeros((570, 805, 3), np.uint8)

cv2.rectangle(image, (50, 50), (755, 530), (62, 184, 22), -1)

# Draw a polygon
pts = np.array(
        [
            [402,110],
            [689,285],
            [403,466],
            [114,285]
        ], 
        np.int32
)
pts = pts.reshape((-1,1,2))
cv2.fillPoly(image,[pts],(31,225, 255), 8, 0)

cv2.circle(image, (403, 285), 132, (184, 81, 22), -1, 2)
# cv2.drawDetectedDiamonds(image, (10,100,150,300), (15, 75, 50))



cv2.imshow("Retangle", image)
cv2.imshow("Retangle", image)
# cv2.imwrite("./img/Retangle.jpg", image)

cv2.waitKey(0)
# cv2.destroyAllWindows()
