# Questão 3a - Djanilson Alves
import cv2
import numpy as np
from matplotlib import pyplot as plot

image = cv2.imread('./equaliza.png')
# grayImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Image", image)
# hist = cv2.calcHist([image], [0], None, [256], [0, 256])
plot.hist(image.ravel(), 256, [0,256])
plot.savefig("./Histograma.jpg")
plot.show()

cv2.waitKey(0)
cv2.destroyAllWindows()

