# Questão 3b - Djanilson Alves
import cv2
import numpy as np
from matplotlib import pyplot as plot

image = cv2.imread('./equaliza.png', 0)
equ = cv2.equalizeHist(image)
cv2.imshow("Image", equ)
# hist = cv2.calcHist([image], [0], None, [256], [0, 256])
plot.hist(equ.ravel(), 256, [0,256])
plot.savefig("./HistogramaEqu.jpg")
plot.show()

# cv2.imwrite("./EqualizedImage.jpg", grayImage)

cv2.waitKey(0)
cv2.destroyAllWindows()



