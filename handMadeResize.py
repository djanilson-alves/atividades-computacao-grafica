# get image mtx pix

import numpy as np
import cv2

img = cv2.imread("./tree.jpg")
#        R  C  I
px = img
imm = []
nar = []
# intera sobre as linas
for idx, rList in enumerate(px):
    # se alinha for par
    if idx % 2 == 0:
        # intera sobre as colunas
        for j, cList in enumerate(rList):
            # se a coluna for impar
            if j % 2 != 0:
                nar.append(cList)
    # se alinha for impar
    else:
        # intera sobre as colunas
        for k, kol in enumerate(rList):
            # se a coluna for impar
            if k % 2 == 0:
                nar.append(kol)
    # imm.append(np.matrix(nar))
# print(nar)
print("doing...")

im = np.matrix(nar)
cv2.imshow('nar', im)
cv2.waitKey()
