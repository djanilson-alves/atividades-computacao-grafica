# rescaling imagem
import cv2
import numpy as np

# load our imput image
image = cv2.imread('./louver.jpg')

# let's make our image 3/4 of it's original size
image_scaled = cv2.resize(
    image,
    None,
    fx=0.25,
    fy=0.25,
    interpolation=cv2.INTER_LINEAR
)

# cv2.imshow('o_image', image)
cv2.imshow('Scaling_image_linear_interpolation', image_scaled)
cv2.imwrite('./imglinear.jpg', image_scaled)


image_scaledX2 = cv2.resize(
    image_scaled,
    None,
    fx=2.5,
    fy=2.5,
    interpolation=cv2.INTER_LINEAR
)
cv2.imshow('2xScaling_image_linear_interpolation', image_scaledX2)


# Aplica os respectivos filtros
kernel = np.ones((6, 6), np.float32)/25
filter2D = cv2.filter2D(image_scaledX2,-1, kernel)
# blur = cv2.blur(image_scaledX2,(5,5))
gaussianBlur = cv2.GaussianBlur(image_scaledX2,(5,5),0)
# cv2.imshow('B2xScaling_image_linear_interpolation', blur)
cv2.imshow('G2xScaling_image_linear_interpolation', gaussianBlur)

cv2.waitKey()