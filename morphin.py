import cv2
import numpy as np

image = cv2.imread('./scan.jpg', 0)
cv2.imshow('Original', image)
cv2.waitKey(0)

# cordinates of the 4 cornes of original image
points_a = np.float32([[320, 15], [700, 215], [85, 610], [530, 780]])

# cordinates of the 4 cornes that us wish
points_b = np.float32([[0, 0], [420, 0], [0, 594], [420, 594]])

M = cv2.getPerspectiveTransform(points_a, points_b)
wraped = cv2.warpPerspective(image, M, (420, 594))
cv2.imshow('wrapePer..', wraped)
cv2.imwrite('./wrapePer.jpg', wraped)
cv2.waitKey(0)
cv2.destroyAllWindows()
