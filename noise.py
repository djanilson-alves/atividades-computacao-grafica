import cv2
import numpy as np

imageruido = cv2.imread("./noise_image.png")
kernel = np.ones((3, 3), np.float32)/9

filteredimage = cv2.filter2D(imageruido, -1, kernel)
cv2.imshow("image original com ruido", imageruido)
cv2.imshow("image filtrada por meidia", filteredimage)
cv2.imwrite("./filteredLena.jpg", filteredimage)
# image original

cv2.waitKey(0)
cv2.destroyAllWindows()

