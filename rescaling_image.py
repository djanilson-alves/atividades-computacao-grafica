# rescaling imagem
import cv2
import numpy as np

#load our imput image
image = cv2.imread('./louver.jpg')

# let's make our image 3/4 of it's original size
image_scaled = cv2.resize(image, None, fx=0.5, fy=0.5)
cv2.imshow('o_image', image)
cv2.imshow('Scaling_image_linear_interpolation', image_scaled)
cv2.imwrite('./imglinear.jpg', image_scaled)
cv2.waitKey()