import cv2
import numpy as np

imgrectangle = cv2.imread('./img/rectangle.jpg')
imgcircle = cv2.imread('./img/cicle.jpg')
imgtext = cv2.imread('./img/text.jpg')

image = imgrectangle + imgcircle + imgtext

cv2.imshow('Image', image)
cv2.imwrite('./img/operacaosoma.jpg', image)

cv2.waitKey(0)

#cv2.destroyAllWindows()

